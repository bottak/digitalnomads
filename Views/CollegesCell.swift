//
//  CollegesCell.swift
//  DigitalNomads
//
//  Created by BOTTAK on 7/3/19.
//  Copyright © 2019 BOTTAK. All rights reserved.
//

import UIKit

class CollegesCell: UITableViewCell {
    
    
    //MARK: - Outlets
    
    @IBOutlet weak var idLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var abbreviationLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
