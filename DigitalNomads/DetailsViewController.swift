//
//  DetailsViewController.swift
//  DigitalNomads
//
//  Created by BOTTAK on 7/3/19.
//  Copyright © 2019 BOTTAK. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    
    //MARK: - Outlets
    
    var details: [Details] = []
    var text: String = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

    
}


extension DetailsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell", for: indexPath) as! DetailsCell
        let detail = details[indexPath.row]
        
        cell.siteLabel.text = detail.site
        cell.college_idLabel.text = String(detail.college_id)
        
        return cell
    }
    
    
}
