//
//  ApiManager.swift
//  DigitalNomads
//
//  Created by BOTTAK on 7/3/19.
//  Copyright © 2019 BOTTAK. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager {
    
    //MARK: - Outlets
    static var instance = ApiManager()
    private enum Constants {
        static let baseURL = "http://asdgroup.pro/tasks/"
    }
    
    private enum EndPoints {
        static let colleges = "colleges.json"
        static let details = "details.json"
        static let ratings = "ratings.json"
    }
    
    //MARK: - Actions
    
    func getColegges(OnComplete: @escaping ([Colleges]) -> Void) {
        let urlSring = Constants.baseURL + EndPoints.colleges
        Alamofire.request(urlSring, method: .get, parameters: [:]).responseJSON { (respons) in
            print(respons)
            switch respons.result {
                
            case .success(let dataColleges):
                print(dataColleges)
                
                if let arrayColleges = dataColleges as? Array<Dictionary<String, Any>> {
                    var colleges: [Colleges] = []
                    for collegesDict in arrayColleges {
                        let college = Colleges()
                        college.id = collegesDict["id"] as? Int ?? 0
                        college.name = collegesDict["name"] as? String ?? ""
                        college.abbreviation = collegesDict["abbreviation"] as? String ?? ""
                        colleges.append(college)
                    }
                    OnComplete(colleges)
                }
                
            case .failure(let error):
                print(error)
            }

            
        }
        
    }
    
    func getDetails(OnComplete: @escaping ([Details]) -> Void) {
        let urlSring = Constants.baseURL + EndPoints.details
        Alamofire.request(urlSring, method: .get, parameters: [:]).responseJSON { (respons) in
            print(respons)
            switch respons.result {
                
            case .success(let dataDelails):
                print(dataDelails)
                
                if let arrayDetails = dataDelails as? Array<Dictionary<String, Any>> {
                    var details: [Details] = []
                    for detailsDict in arrayDetails {
                        let detail = Details()
                        detail.college_id = detailsDict["college_id"] as? Int ?? 0
                        detail.site = detailsDict["site"] as? String ?? ""
                        details.append(detail)
                    }
                    OnComplete(details)
                }
                
            case .failure(let error):
                print(error)
            }
            
            
        }
        
    }
    
    
    func getRatings(OnComplete: @escaping ([Ratings]) -> Void) {
        let urlSring = Constants.baseURL + EndPoints.ratings
        Alamofire.request(urlSring, method: .get, parameters: [:]).responseJSON { (respons) in
            print(respons)
            switch respons.result {
                
            case .success(let dataRatings):
                print(dataRatings)
                
                if let arrayRatings = dataRatings as? Array<Dictionary<String, Any>> {
                    var ratings: [Ratings] = []
                    for ratingsDict in arrayRatings {
                        let rating = Ratings()
                        rating.college_id = ratingsDict["college_id"] as? Int ?? 0
                        rating.rating = ratingsDict["rating"] as? Double ?? 0.0
                        ratings.append(rating)
                    }
                    OnComplete(ratings)
                }
                
            case .failure(let error):
                print(error)
            }
            
            
        }
        
    }
    
    
    
    
}
