//
//  CollegesViewController.swift
//  DigitalNomads
//
//  Created by BOTTAK on 7/3/19.
//  Copyright © 2019 BOTTAK. All rights reserved.
//

import UIKit

class CollegesViewController: UIViewController {
    
    
    //MARK: - Outltes
    
    
    var college: [Colleges] = []
    var text: String = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        ApiManager.instance.getColegges(OnComplete: {(CollegesFromServer) in
            self.tableView.reloadData()
        })

        
    }
    

    //MARK: - Actions

}


extension CollegesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return college.count
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "CollegesViewController", bundle: nil)
//        let stopVC = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
//        collegeVC.id = Details[indexPath.row].college_id
//        navigationController?.pushViewController(collegeVC, animated: true)
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CollegesCell", for: indexPath) as! CollegesCell
        let colleges = college[indexPath.row]
        
        cell.idLabel.text = String(colleges.id)
        cell.nameLabel.text = colleges.name
        cell.abbreviationLabel.text = colleges.abbreviation
        
        return cell
    }
    
    
}
