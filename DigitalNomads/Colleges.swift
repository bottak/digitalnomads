//
//  Colleges.swift
//  DigitalNomads
//
//  Created by BOTTAK on 7/3/19.
//  Copyright © 2019 BOTTAK. All rights reserved.
//

import Foundation

class Colleges {
    
    var id: Int = 0
    var name: String = ""
    var abbreviation: String = ""
}
